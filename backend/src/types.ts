export type Email = string;
export type Password = string;
export type HashedPassword = string;
export type PhoneNumber = string;
export type NonEmptyString = string;
export type UserId = number;
