import { Router } from "express"
import { authMiddleware } from "../auth/auth";
import { contactInfoHandler, editUserDetailsHandler, getMyUserDetailsHandler, loginHandler, registerHandler } from "./Users.handlers";

export const createUsersRouter = () => {
    const router = Router();
    router.post('/register', registerHandler);
    router.post('/login', loginHandler);
    router.post('/edit', authMiddleware, editUserDetailsHandler);
    router.get('/me', authMiddleware, getMyUserDetailsHandler);
    router.get('/contactInfo/:id', contactInfoHandler);

    return router;
}
