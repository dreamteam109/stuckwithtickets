import { Router, Request } from "express"
import { authMiddleware } from "../auth/auth";
import { addImageHandler, createMulterMiddleware, deleteImageHandler, getImageHandler } from "./Images.handlers";

export const createImagesRouter = () => {
    const router = Router();

    router.post('/add', createMulterMiddleware(), addImageHandler); // TODO figure out if needed or will be done in ticket endpoint
    router.get('/:id', getImageHandler);
    router.delete('/:id', authMiddleware, deleteImageHandler); // TODO figure out if needed or will be done in ticket endpoint

    return router;
}
