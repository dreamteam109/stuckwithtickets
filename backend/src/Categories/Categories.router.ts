import { Router } from "express";
import { getCategoriesHandler } from "./Categories.handlers";

export const createCategoriesRouter = () => {
    const router = Router();
    router.get('/', getCategoriesHandler);
    return router;
}