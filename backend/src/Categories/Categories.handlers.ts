import { Request, Response } from "express";
import { getAllCategories } from "../db/categories";

export const getCategoriesHandler = async (req: Request, res: Response) => {
    const categories = await getAllCategories();
    res.json(categories);
}
