export interface CommonColumns {
    id: number;
    isDeleted: boolean;
    lastUpdateDate: Date;
    creationDate: Date;
}

export interface ImageRecord {
    id: string;
    mimeType: string;
    imageData: string;
}
