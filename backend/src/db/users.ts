import { User, UserForCreate, UserForEdit } from "../Users/Users.types";
import pool from "./pool";

export const createUser = async (user: UserForCreate): Promise<void> => {
  const { email, password, firstName, lastName, phoneNumber, imageId } = user;

  await pool.query(`
    insert into stuckwithtickets.users (first_name, last_name, email, password, phone_number, image_id)
    values ($1, $2, $3, $4, $5, $6)
  `, [firstName, lastName, email, password, phoneNumber, imageId]
  );
};

export const editUserDetails = async (user: UserForEdit): Promise<void> => {
  const { id, firstName, lastName, phoneNumber, imageId } = user;

  await pool.query(
    ` update stuckwithtickets.users 
     set first_name = $2,
         last_name = $3,
         phone_number = $4,
         image_id = $5,
         last_update_date = now()
        where id = $1 and is_deleted = false
 `
    , [id, firstName, lastName, phoneNumber, imageId]);
}

export type UserInfoForAuth = Omit<User, "isDeleted" | "lastUpdateDate" | "creationDate">;

export const getUserByEmailForAuth = async (email: string): Promise<UserInfoForAuth> => {
  const { rows }: { rows: UserInfoForAuth[] } = await pool.query(`
    select
        id,
        first_name as "firstName",
        last_name as "lastName",
        email,
        password,
        phone_number as "phoneNumber",
        image_id as "imageId"
    from stuckwithtickets.users
    where is_deleted = false
        and email = $1
  `, [email]
  );
  return rows[0];
};

export type ContactInfo = Omit<User, "password" | "isDeleted" | "lastUpdateDate" | "creationDate">;

export const getContactInfoById = async (userId: number): Promise<ContactInfo> => {
  const { rows }: { rows: ContactInfo[] } = await pool.query(`
    select
      id,
      first_name as "firstName",
      last_name as "lastName",
      email,
      phone_number as "phoneNumber",
      image_id as "imageId"
    from stuckwithtickets.users
    where is_deleted = false
      and id = $1
  `, [userId]
  );
  return rows[0];
}
