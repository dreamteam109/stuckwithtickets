package com.dreamteam109.stuckwithtickets.model.restapi;

import android.util.Log;

import com.dreamteam109.stuckwithtickets.model.restapi.apis.CategoriesApi;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.ImagesApi;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.TicketsApi;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.UsersApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class MyRetrofit {
    public static final String BASE_URL = "http://10.0.2.2:8080/";
    private static MyRetrofit myRetrofit;
    private Retrofit retrofit;

    public MyRetrofit() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                .create();

        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        Log.d("StuckWithTicketsLog", "MyRetrofit - MyRetrofit");
    }

    public static synchronized MyRetrofit getInstance() {
        if (myRetrofit == null) {
            myRetrofit = new MyRetrofit();
            Log.d("StuckWithTicketsLog", "MyRetrofit - getInstance - created instance");
        }
        return myRetrofit;
    }

    public CategoriesApi getCategoriesApi() {
        Log.d("StuckWithTicketsLog", "MyRetrofit - getCategoriesApi");
        return retrofit.create(CategoriesApi.class);
    }

    public TicketsApi getTicketsApi() {
        Log.d("StuckWithTicketsLog", "MyRetrofit - getTicketsApi");
        return retrofit.create(TicketsApi.class);
    }

    public UsersApi getUsersApi() {
        Log.d("StuckWithTicketsLog", "MyRetrofit - getUsersApi");
        return retrofit.create(UsersApi.class);
    }

    public ImagesApi getImagesApi() {
        Log.d("StuckWithTicketsLog", "MyRetrofit - getImagesApi");
        return retrofit.create(ImagesApi.class);
    }

}
