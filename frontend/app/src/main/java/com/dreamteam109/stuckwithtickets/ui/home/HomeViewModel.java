package com.dreamteam109.stuckwithtickets.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

import java.util.List;

public class HomeViewModel extends ViewModel {
    private LiveData<List<Ticket>> tickets = Transformations.distinctUntilChanged(Model.instance.getAllTickets());

    public LiveData<List<Ticket>> getTicketsList() {
        return tickets;
    }

    public void setSearchString(String searchString) {
        Model.instance.setSearchString(searchString);
    }
}