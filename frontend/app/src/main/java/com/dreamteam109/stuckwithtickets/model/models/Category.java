package com.dreamteam109.stuckwithtickets.model.models;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    public Category() {
        Log.d("StuckWithTicketsLog", "Category - empty constructor");
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
        Log.d("StuckWithTicketsLog", "Category - constructor " + this.toString());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
