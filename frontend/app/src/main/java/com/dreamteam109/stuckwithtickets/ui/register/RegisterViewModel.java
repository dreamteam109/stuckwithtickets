package com.dreamteam109.stuckwithtickets.ui.register;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.Validators;

public class RegisterViewModel extends ViewModel {

    private MutableLiveData<RegisterFormState> registerFormState = new MutableLiveData<>();
    private MutableLiveData<Bitmap> bitmap = new MutableLiveData<Bitmap>();
    private MutableLiveData<RegisterResult> registerResult = new MutableLiveData<>();

    public RegisterViewModel() {
    }

    LiveData<RegisterFormState> getRegisterFormState() {
        return registerFormState;
    }

    LiveData<RegisterResult> getRegisterResult() {
        return registerResult;
    }

    LiveData<Bitmap> getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap mBitmap) {
        this.bitmap.setValue(mBitmap);
    }

    public void register(String firstName, String lastName, String email, String password, String phone) {
        Model.RegisterListener listener = new Model.RegisterListener() {
            @Override
            public void onSuccess() {
                registerResult.setValue(new RegisterResult());
            }

            @Override
            public void onFail(String message) {
                registerResult.setValue(new RegisterResult(message));
            }
        };

        if (bitmap.getValue() == null) {
            Model.instance.register(firstName, lastName, email, password, phone, null, listener);
        } else {
            Model.instance.addImage(bitmap.getValue(), new Model.AddImageListener() {
                @Override
                public void onComplete(String imageId) {
                    Model.instance.register(firstName, lastName, email, password, phone, imageId, listener);
                }
            });
        }
    }

    public void registerDataChanged(String firstName, String lastName, String email, String password, String verifyPassword, String phone) {
        if (!(firstName != null && !firstName.trim().isEmpty())) {
            registerFormState.setValue(new RegisterFormState(R.string.invalid_first_name, null, null, null, null, null));
        } else if (!(lastName != null && !lastName.trim().isEmpty())) {
            registerFormState.setValue(new RegisterFormState(null, R.string.invalid_last_name, null, null, null, null));
        } else if (!Validators.isEmailValid(email)) {
            registerFormState.setValue(new RegisterFormState(null, null, R.string.invalid_email, null, null, null));
        } else if (!Validators.isPasswordValid(password)) {
            registerFormState.setValue(new RegisterFormState(null, null, null, R.string.invalid_password, null, null));
        } else if (!password.equals(verifyPassword)) {
            registerFormState.setValue(new RegisterFormState(null, null, null, null, R.string.invalid_verify_password, null));
        } else if (!Validators.isPhoneValid(phone)) {
            registerFormState.setValue(new RegisterFormState(null, null, null, null, null, R.string.invalid_phone));
        } else {
            registerFormState.setValue(new RegisterFormState(true));
        }
    }
}