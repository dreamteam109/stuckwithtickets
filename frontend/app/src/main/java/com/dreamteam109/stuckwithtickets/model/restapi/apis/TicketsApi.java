package com.dreamteam109.stuckwithtickets.model.restapi.apis;

import com.dreamteam109.stuckwithtickets.model.models.Ticket;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.EditTicketRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.SearchTicketsRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.SellTicketRequestBody;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TicketsApi {
    @POST("tickets/search")
    Call<List<Ticket>> searchTickets(@Body SearchTicketsRequestBody requestBody);

    @GET("tickets/{id}")
    Call<Ticket> getTicketById(@Path("id") int id);

    @POST("tickets/sell")
    Call<Void> sellTicket(@Header("Authorization") String header, @Body SellTicketRequestBody requestBody);

    @GET("tickets/me")
    Call<List<Ticket>> getMyTickets(@Header("Authorization") String header);

    @POST("tickets/edit/{id}")
    Call<Void> editTicket(@Header("Authorization") String header, @Path("id") int id, @Body EditTicketRequestBody requestBody);

    @DELETE("tickets/{id}")
    Call<Void> deleteTicket(@Header("Authorization") String header, @Path("id") int id);
}
