package com.dreamteam109.stuckwithtickets.model.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.dreamteam109.stuckwithtickets.model.models.Ticket;

import java.util.List;

@Dao
public interface TicketDao {

    @Query("select * from Ticket where isDeleted = 0 order by eventDateTime asc")
    List<Ticket> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Ticket... tickets);

    @Delete
    void delete(Ticket ticket);
}
