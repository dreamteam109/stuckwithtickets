package com.dreamteam109.stuckwithtickets.ui.contactinfo;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.databinding.FragmentContactInfoBinding;
import com.dreamteam109.stuckwithtickets.model.Model;

public class ContactInfoFragment extends Fragment {

    private ContactInfoViewModel contactInfoViewModel;
    private FragmentContactInfoBinding binding;

    ImageView userImv;
    TextView firstNameTv;
    TextView lastNameTv;
    TextView phoneTv;
    TextView emailTv;
    ProgressBar progressBar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentContactInfoBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        firstNameTv = root.findViewById(R.id.contact_first_name_field_tv);
        lastNameTv = root.findViewById(R.id.contact_last_name_field_tv);
        phoneTv = root.findViewById(R.id.contact_phone_field_tv);
        emailTv = root.findViewById(R.id.contact_email_field_tv);
        userImv = root.findViewById(R.id.avatar_contact_imv);
        progressBar = root.findViewById(R.id.contact_progressbar);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        progressBar.setVisibility(View.VISIBLE);

        int userId = ContactInfoFragmentArgs.fromBundle(getArguments()).getUserId();
        contactInfoViewModel = new ViewModelProvider(this, new ContactInfoViewModel.Factory(getActivity().getApplication(), userId))
                .get(ContactInfoViewModel.class);

        contactInfoViewModel.getContactInfo().observe(getViewLifecycleOwner(), contactInfo -> {
            if (contactInfo != null) {
                firstNameTv.setText(contactInfo.getFirstName());
                lastNameTv.setText(contactInfo.getLastName());
                phoneTv.setText(contactInfo.getPhoneNumber());
                emailTv.setText(contactInfo.getEmail());
                String imageId = contactInfo.getImageId();

                Glide.with(ContactInfoFragment.this)
                        .load(Model.instance.getFullImageUrl(imageId))
                        .fitCenter()
                        .placeholder(R.drawable.avatar)
                        .fallback(R.drawable.avatar)
                        .into(userImv);

                progressBar.setVisibility(View.GONE);
            }
        });

    }

}