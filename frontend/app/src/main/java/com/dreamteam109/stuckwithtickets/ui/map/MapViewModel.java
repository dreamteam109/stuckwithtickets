package com.dreamteam109.stuckwithtickets.ui.map;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

import java.util.List;

public class MapViewModel extends ViewModel {
    public LiveData<List<Ticket>> getTicketsList() {
        return Model.instance.getAllTickets();
    }

}
