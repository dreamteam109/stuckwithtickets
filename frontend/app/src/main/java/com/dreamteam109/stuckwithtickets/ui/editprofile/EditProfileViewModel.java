package com.dreamteam109.stuckwithtickets.ui.editprofile;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.User;

public class EditProfileViewModel extends AndroidViewModel {
    private LiveData<User> user;
    private MutableLiveData<String> firstName = new MutableLiveData<String>();
    private MutableLiveData<String> lastName = new MutableLiveData<String>();
    private MutableLiveData<String> phoneNumber = new MutableLiveData<String>();
    private MutableLiveData<Bitmap> bitmap = new MutableLiveData<Bitmap>();

    private boolean bitmapWasSet = false;

    public EditProfileViewModel(@NonNull Application application) {
        super(application);
        user = Model.instance.getProfileInfo();
    }


    public LiveData<User> getUser() {
        return user;
    }

    public MutableLiveData<String> getFirstName() {
        return firstName;
    }

    public MutableLiveData<String> getLastName() {
        return lastName;
    }

    public MutableLiveData<String> getPhoneNumber() {
        return phoneNumber;
    }

    public MutableLiveData<Bitmap> getBitmap() {
        return bitmap;
    }

    public void setFirstName(String mFirstName) {
        this.firstName.setValue(mFirstName);
    }

    public void setLastName(String mLastName) {
        this.lastName.setValue(mLastName);
    }

    public void setPhoneNumber(String mPhone) {
        this.phoneNumber.setValue(mPhone);
    }

    public void setBitmap(Bitmap mBitmap) {
        this.bitmapWasSet = true;
        this.bitmap.setValue(mBitmap);
    }

    public void editProfile(Model.EditProfileListener listener) {
        if (!bitmapWasSet) {
            Model.instance.editProfile(user.getValue().id,
                    firstName.getValue(),
                    lastName.getValue(),
                    phoneNumber.getValue(),
                    getUser().getValue().getImageId(),
                    listener);

        } else {
            if (bitmap.getValue() == null) {
                Model.instance.editProfile(user.getValue().id,
                        firstName.getValue(),
                        lastName.getValue(),
                        phoneNumber.getValue(),
                        null,
                        listener);
            } else {
                Model.instance.addImage(bitmap.getValue(), new Model.AddImageListener() {
                    @Override
                    public void onComplete(String imageId) {
                        Model.instance.editProfile(
                                user.getValue().getId(),
                                firstName.getValue(),
                                lastName.getValue(),
                                phoneNumber.getValue(),
                                imageId,
                                listener);
                    }
                });
            }
        }
    }

    public static class Factory implements ViewModelProvider.Factory {

        @NonNull
        private final Application application;

        public Factory(@NonNull Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> aClass) {
            return (T) new EditProfileViewModel(application);
        }
    }
}

