package com.dreamteam109.stuckwithtickets.ui.login;

import androidx.annotation.Nullable;

class LoginResult {
    private boolean isSuccessful;
    @Nullable
    private String errorMessage;

    LoginResult() {
        this.errorMessage = null;
        this.isSuccessful = true;
    }

    LoginResult(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
        this.isSuccessful = false;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    @Nullable
    String getErrorMessage() {
        return errorMessage;
    }
}